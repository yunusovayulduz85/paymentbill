import React, { useState } from 'react'

const AddTodo = ({addTodo}) => {
    const [value,setValue]=useState("")
  return (
    <div className='container text-center'>
        <input type='text' value={value} onChange={(e)=>setValue(e.target.value)}/>
        <button className='btn btn-primary' onClick={()=>addTodo(value)}>add</button>
    </div>
  )
}

export default AddTodo;