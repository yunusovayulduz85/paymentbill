import React, { useContext } from 'react'
import { TaskContext } from '../App';

const Todo = () => {
    const state = useContext(TaskContext)
    return (
        <div className='container text-center'>
            <h1>Todos:{state.length}</h1>
        </div>
    )
}

export default Todo;