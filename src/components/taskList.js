import React, { useContext } from 'react'
import { TaskContext } from '../App'

const TaskList = () => {
    const state = useContext(TaskContext);
    return (
        <div>
            <ul>
                {
                    state.map((item, index) => {
                        return <>
                            <li key={index}> {index + 1}) {item}</li>
                        </>
                    })
                }
            </ul>
        </div>
    )
}

export default TaskList