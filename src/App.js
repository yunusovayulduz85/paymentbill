import React, { useState } from 'react'
const initialFriends = [
  {
    id: 118836,
    name: "Clark",
    image: "https://i.pravatar.cc/48?u=118836",
    balance: -7,
  },
  {
    id: 933372,
    name: "Sarah",
    image: "https://i.pravatar.cc/48?u=933372",
    balance: 20,
  },
  {
    id: 499476,
    name: "Anthony",
    image: "https://i.pravatar.cc/48?u=499476",
    balance: 0,
  },
];
function Button({ children, onClick }) {
  return <button className='button' onClick={onClick}>{children}</button>
}
export default function App() {
  const [friends, setFriends] = useState(initialFriends);
  const [showAddFriend, setShowAddFriend] = useState(false)
  const [selectFriend,setSelectFriend]=useState(null)
  const handleAddFriend = (friend) => {
    setFriends(prev => [...prev, friend]);
    setShowAddFriend(false)
  }
  const handleSubmit=(value)=>{
    for (let i = 0; i < friends.length; i++) {
      if(selectFriend.id==friends[i].id){
        friends[i].balance+=value;
        
      }
      setFriends([...friends]);
      setShowAddFriend(false)
    }
  }
  return (
    <div>
      <div className='app'>
        <div className='sidebar'>
          <FriendList friends={friends} onSelect={setSelectFriend} />
          {
            showAddFriend && <FormAddFriend onAddFriend={handleAddFriend} />
          }

          <Button onClick={() => setShowAddFriend(true)}>Add New Friend</Button>
        </div>
        {
          selectFriend && <FromSplitBill onSubmit={handleSubmit} selectFriend={selectFriend}/>
        }
        
      </div>
    </div>
  )
}
function FriendList({ friends,onSelect }) {
  return <>
    {
      friends.map((friend, index) => <Friend friend={friend} key={index} onSelect={onSelect}/>)
    }
  </>
}
function Friend({ friend,onSelect }) {
  return <>
    <li>
      <img src={friend.image} alt={friend.id} />
      <h3>{friend.name}</h3>
      {friend.balance < 0 ? (<p className='red'>Siznig qarzingiz : {friend.balance}$</p>):""}
      {friend.balance > 0 ? (<p className='green'>Do'stingizni qarzi : {friend.balance}$</p>):""}
      {friend.balance == 0 ? (<p>Qarzlar yo'q</p>):""}
      <Button onClick={()=>onSelect(friend)}>Select</Button>

    </li>
  </>
}
function FormAddFriend({ onAddFriend }) {
  const [name, setName] = useState("");
  const [img, setImg] = useState("https://picsum.photos/48/48");

  const handleSubmit = () => {
    if (!name || !img) {
      return;
    }
    const newFriend = {
      id:Date.now(),
      name,
      image: img,
      balance: 0,
    }
    onAddFriend(newFriend);
  }
  return <>
    <form action='#' className='form-add-friend'>
      <label>
        Friend Name
        <input type='text' value={name} onChange={(e) => setName(e.target.value)} />
      </label>
      <br />
      <label>
        Friend Photo
        <input type='text' value={img} onChange={(e) => setImg(e.target.value)} />
      </label>
      <br />
      <Button onClick={handleSubmit}>Add</Button>
    </form>
  </>
}
function FromSplitBill({onSubmit,selectFriend}) {
  const [totalSum, setTotalSum] = useState("")
  const [yourTotal, setYourTotal] = useState("")
  const friendTotal = totalSum ? totalSum - yourTotal :"";
  const [whoIsPaying, setWhoIsPaying] = useState("user")
  const handleSubmitBill=(e)=>{
    e.preventDefault();
    onSubmit(whoIsPaying=="user" ? friendTotal : -yourTotal)
  }
  return <>
    <form className='form-split-bill'>
      <h3>Billing with {selectFriend.name}👩🏻‍🤝‍👩🏻</h3>
      <br />
      <label>Total amount💸</label>
      <input type='text' value={totalSum} onChange={(e) => setTotalSum(e.target.value)} />
      <label>Your payment💵</label>
      <input type='text' value={yourTotal} onChange={(e) => setYourTotal(Number(e.target.value))} />
      <label>{selectFriend.name}'s payoff💵</label>
      <input type='text' value={friendTotal} />
      <label>Who pays the check?📑</label>
      <select value={whoIsPaying} onChange={(e) => setWhoIsPaying(e.target.value)}>
        <option value="user">Men</option>
        <option value="friend">{selectFriend.name}</option>
      </select>
      <Button onClick={handleSubmitBill}>Hisobni to'lash</Button>
    </form>
  </>
  

}